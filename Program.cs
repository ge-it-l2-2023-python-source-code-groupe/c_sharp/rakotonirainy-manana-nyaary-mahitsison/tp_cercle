﻿using System;

//program main
Console.Write("Donner l'abscisse du centre :");
double x = double.Parse(Console.ReadLine() ?? "");
Console.Write("Donner l'ordonné du centre : ");
double y = double.Parse(Console.ReadLine() ?? "");
Console.Write("Donner le rayon : ");
double r = double.Parse(Console.ReadLine() ?? "");

//création d'un cercle
Cercle cercle = new Cercle(x, y, r);
cercle.Display();
cercle.Perimetre();
cercle.Surface();

//input point à verifier
Console.WriteLine("Donner un point : ");
Console.Write("X: ");
int monx = int.Parse(Console.ReadLine() ?? "");
Console.Write("Y : ");
int mony = int.Parse(Console.ReadLine() ?? "");

//creation d'un point
Point point = new Point(monx, mony);
point.View();

//verification de l'appartenance du point 
point.VerificationPoint(monx, mony, x, y, r);



//class cercle
class Cercle
{
    public double X { get; set; }
    public double Y { get; set; }
    public double R { get; set; }

    private double pi = 3.14;

    public Cercle(double x, double y, double r)
    {
        X = x;
        Y = y;
        R = r;
    }

    public void Display()
    {
        Console.WriteLine($"Cercle ({X},{Y},{R})");
    }

    public void Perimetre()
    {
        Console.WriteLine($"Le perimetre est : {pi * 2 * R}");
    }
    public void Surface()
    {
        Console.WriteLine($"La surface est : {pi * (R * R)}");
    }
}


//class Point 
class Point
{
    public double monx { get; set; }
    public double mony { get; set; }



    public Point(double monx, double mony)
    {
        this.monx = monx;
        this.mony = mony;

    }

    public void View()
    {
        Console.WriteLine($"Point ({monx},{mony})");

    }

    public void VerificationPoint(int monx, int mony, double X, double Y, double R)
    {
        double distance = Math.Sqrt(Math.Pow(monx - X, 2) + Math.Pow(mony - Y, 2));

        if (distance <= R)
        {
            Console.WriteLine($"Le point ({monx},{mony}) est à l'interieur du cercle : ({X},{Y},{R}) ");
        }
        else
        {
            Console.WriteLine($"Le point ({monx},{mony}) n'est pas à l'interieur du cercle  : ({X},{Y},{R}) ");
        }
    }


}